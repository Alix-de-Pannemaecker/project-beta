import logo_ae_auto from '../logo_ae_auto.png'
import React from 'react';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';


function NavBar() {
  return (
    <Navbar collapseOnSelect bg="dark" expand="lg" variant="dark">
      <Container>
        <Navbar.Brand href="/">
          <img
            src={logo_ae_auto}
            width="75"
            height="68"
          />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className='me-auto'>
            <NavDropdown title="Inventory" id="collapsible-nav-dropdown">
              <NavDropdown.Item id="nav-dropdown-item" href="/manufacturers">Manufacturers</NavDropdown.Item>
              <NavDropdown.Item id="nav-dropdown-item" href="/manufacturers/new">Add Manufacturer</NavDropdown.Item>
              <NavDropdown.Item id="nav-dropdown-item" href="/models">Models</NavDropdown.Item>
              <NavDropdown.Item id="nav-dropdown-item" href="/models/new">Add Model</NavDropdown.Item>
              <NavDropdown.Item id="nav-dropdown-item" href="/autos">Automobiles</NavDropdown.Item>
              <NavDropdown.Item id="nav-dropdown-item" href="/autos/new">Add Automobile</NavDropdown.Item>
            </NavDropdown>
            <NavDropdown title="Sales" id="collapsible-nav-dropdown">
              <NavDropdown.Item id="nav-dropdown-item" href="/sales/new">Add Sales Representative</NavDropdown.Item>
              <NavDropdown.Item id="nav-dropdown-item" href="/customers/new">Add Customer</NavDropdown.Item>
              <NavDropdown.Item id="nav-dropdown-item" href="/records/new">Add Sales Record</NavDropdown.Item>
              <NavDropdown.Item id="nav-dropdown-item" href="/records">View Sales Records</NavDropdown.Item>
              <NavDropdown.Item id="nav-dropdown-item" href="/records/history">View Sales by Representative</NavDropdown.Item>
              <NavDropdown.Item id="nav-dropdown-item" href="/sales/stats">View Sales Statistics</NavDropdown.Item>
            </NavDropdown>
            <NavDropdown title="Services" id="collapsible-nav-dropdown">
              <NavDropdown.Item id="nav-dropdown-item" href="/appointments">View Appointments</NavDropdown.Item>
              <NavDropdown.Item id="nav-dropdown-item" href="/appointments/new">Add Appointment</NavDropdown.Item>
              <NavDropdown.Item id="nav-dropdown-item" href="/appointments/history">View Appointment History</NavDropdown.Item>
              <NavDropdown.Item id="nav-dropdown-item" href="/appointments/stats">View VIP Service Appointments</NavDropdown.Item>
              <NavDropdown.Item id="nav-dropdown-item" href="/technicians/new">Add Technician</NavDropdown.Item>
            </NavDropdown>
            <NavDropdown title="Rentals" id="collapsible-nav-dropdown">
              <NavDropdown.Item id="nav-dropdown-item" href="/rentals/cars">View Rental Cars</NavDropdown.Item>
              <NavDropdown.Item id="nav-dropdown-item" href="/rentals/cars/new">Add Cars</NavDropdown.Item>
              <NavDropdown.Item id="nav-dropdown-item" href="/rentals/bookings">View Rental Bookings</NavDropdown.Item>
              <NavDropdown.Item id="nav-dropdown-item" href="/rentals/bookings/new">Add Rental Booking</NavDropdown.Item>
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}

export default NavBar;
