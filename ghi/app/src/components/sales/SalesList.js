import React, { useState, useEffect } from 'react';

function SalesList() {
    const [salesRecords, setSalesRecords] = useState([]);

    const getData = async () => {
        const resp = await fetch('http://localhost:8090/api/records/');
        if (resp.ok) {
            const data = await resp.json();
            setSalesRecords(data.sales_records);
        }
    };

    useEffect(() => {
        getData();
    }, []);

    return (
        <div id="form-row" className="row">
        <div className='container-responsive shadow p-4'>
            <h1 className='text-center'>Sales</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Representative</th>
                        <th>Employee Number</th>
                        <th>Customer</th>
                        <th>Automobile VIN</th>
                        <th>Price | USD</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        salesRecords.map(salesRecord => {
                            return (
                                <tr key={salesRecord.id}>
                                    <td>{ salesRecord.sales_rep_name}</td>
                                    <td>{ salesRecord.sales_rep}</td>
                                    <td>{ salesRecord.customer_name}</td>
                                    <td>{ salesRecord.vin}</td>
                                    <td>${ salesRecord.price}</td>
                                </tr>
                            );
                        })
                    }
                </tbody>
            </table>
        </div>
        </div>
    );
}

export default SalesList;
