import React, { useState, useEffect } from 'react';

function SalesForm() {
    const [vins, setVin] = useState([]);
    const [salesReps, setSalesReps] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [formData, setFormData] = useState({
        vin: '',
        sales_rep: '',
        customer: '',
        price: ''
    });

    const getInventoryData = async () => {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            const inventory = data.autos.map((auto) => {
                return auto.vin;
            });
            setVin(inventory);
        }
    };

    useEffect(() => {
        getInventoryData();
    }, []);

    const getSalesRepsData = async () => {
        const url = 'http://localhost:8090/api/sales/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            const salesReps = data.sales_reps.map(salesRep => {
                return `${salesRep.name} ${salesRep.employee_number}`;
            });
            setSalesReps(salesReps);
        }
    }

    useEffect(() => {
        getSalesRepsData();
    }, []);

    const getCustomersData = async () => {
        const url = 'http://localhost:8090/api/customers/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            const customers = [];
            for (const customer of data.customers) {
                if (!customers.includes(`${customer.name} ${customer.id}`)) {
                    customers.push(`${customer.name} ${customer.id}`);
                }
            }
            setCustomers(customers);
        }
    };

    useEffect(() => {
        getCustomersData();
    }, []);


    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        const salesRecordUrl = 'http://localhost:8090/api/records/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(salesRecordUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                vin: '',
                sales_rep: '',
                customer: '',
                price: ''
            });
        }
    };

    return (
        <div className="row">
            <div className="offset-3 col-6 mb-5 mt-5">
                <div className="shadow p-4 mt-4">
                    <h1 className='text-center'>Sales Record Form</h1>
                    <form onSubmit={handleSubmit} id="add-sale-form">
                        <div className="mb-3">
                            <select onChange={handleFormChange} value={formData.vin} required name="vin" id="vin" className="form-select">
                                <option value="">Choose a vehicle</option>
                                {
                                    vins.map(vin => {
                                        return (
                                            <option key={vin} value={vin}>
                                                {vin}
                                            </option>
                                        );
                                    })
                                }
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleFormChange} value={formData.sales_rep} required name="sales_rep" id="sales_rep" className="form-select">
                                <option>Choose a sales representative</option>
                                {
                                    salesReps.map(salesRep => {
                                        const salesRepId = parseInt(salesRep.replace(/[^0-9]/g, ''));
                                        const salesRepName = salesRep.replace(salesRepId, '').trim();
                                        return (
                                            <option key={salesRepId} value={salesRepId}>
                                                {salesRepName}
                                            </option>
                                        );
                                    })
                                }
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleFormChange} value={formData.customer} required name="customer" id="customer" className="form-select">
                                <option>Choose a customer</option>
                                {
                                    customers.map(customer => {
                                        const customerId = parseInt(customer.replace(/[^0-9]/g, ''));
                                        const customerName = customer.replace(customerId, '').trim();
                                        return (
                                            <option key={customerId} value={customerId}>
                                                {customerName}
                                            </option>
                                        );
                                    })
                                }
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.price} placeholder='Price' required type="number" name="price" id="price" className="form-control" />
                            <label htmlFor="price">Price</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default SalesForm;
