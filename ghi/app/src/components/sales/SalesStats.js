import React, {useState, useEffect } from 'react';
import { Chart } from 'react-google-charts';

function SalesStats() {
    const [salesPercentageData, setPercentageSalesData] = useState([]);
    const [options, setOptions] = useState({
        title: "Percentage Sales By Representative",
    });
    const [salesDollarData, setSalesDollarData] = useState([]);
    const [dollarOptions, setDollarOptions] = useState({
        title: "Sales Representatives Sales in Dollars"
    });

    const getData = async () => {
        const resp = await fetch('http://localhost:8090/api/records/');
        if (resp.ok) {
            const data = await resp.json();
            const salesReps = [];
            const salesRepsNames = [];
            for (const salesRecord of data.sales_records) {
                if (!(salesReps.includes(salesRecord.sales_rep))) {
                    salesReps.push(salesRecord.sales_rep);
                    salesRepsNames.push(salesRecord.sales_rep_name)
                }
            }
            console.log(salesReps);
            const repsTotalSales = {};
            for (const salesRecord of data.sales_records) {
                const salesRep = salesRecord.sales_rep;
                    if (!(salesRep in repsTotalSales)) {
                        repsTotalSales[salesRep] = salesRecord.price;
                    } else {
                        repsTotalSales[salesRep] += salesRecord.price;
                    }
            }
            // total = 620400
            // const totalSales = Object.values(repsTotalSales);
            const salesData = [["Sales Rep Name", "Total Sales", "Sales Rep"]];
            const salesDollarData = [["Sales Rep Name", "Total Sales"]];
            for (let i = 0; i < salesReps.length; i++) {
                salesData.push([salesRepsNames[i], repsTotalSales[salesReps[i]], salesReps[i]]);
                salesDollarData.push([salesRepsNames[i], repsTotalSales[salesReps[i]]]);
            }
            setPercentageSalesData(salesData);
            setSalesDollarData(salesDollarData);
        }
    };

    useEffect(() => {
        getData();
    }, []);

    return (
        <>
        <div className='bg-seondary-subtle pb-5 mb-5'>
        <div id='stats-div' className='container shadow col mt-4 me-auto ms-auto card h-100 mb-5 overflow-scroll'>
            <h1 className='text-center pt-3'>Sales By Representative</h1>
            <Chart
                chartType='PieChart'
                width='100%'
                height='400px'
                data={salesPercentageData}
                options={options}
            />
             <Chart className='pb-5 mb-5'
                chartType='BarChart'
                width='100%'
                height='400px'
                data={salesDollarData}
                options={dollarOptions}
            />
        </div>
        </div>
        </>
    )
}

export default SalesStats;
