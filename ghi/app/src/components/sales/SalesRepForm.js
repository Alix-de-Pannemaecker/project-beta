import React, { useState } from 'react';

function SalesRepForm() {
    const [formData, setFormData] = useState({
        name: '',
        employee_number: ''
    });

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        const salesRepsUrl = 'http://localhost:8090/api/sales/';

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            }
        };

        const response = await fetch(salesRepsUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                name: '',
                employee_number: ''
            });
        }
    };

    return (
        <div id="form-row" className="row">
            <div className="offset-3 col-6 mb-5 mt-5">
                <div className="shadow p-4 mt-4">
                    <h1 className='text-center'>Add Sales Representative</h1>
                    <form onSubmit={handleSubmit} id="add-sales-rep">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.name} placeholder='Name' required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.employee_number} placeholder='Employee Number' required type="number" name="employee_number" id="employee_number" className="form-control"/>
                            <label htmlFor="name">Employee Number</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SalesRepForm;
