import React, { useState, useEffect } from 'react';

function SalesRepHx() {
    const [salesRecords, setSalesRecords] = useState([]);
    const [salesReps, setSalesReps] = useState([]);
    const [filterSalesRep, setFilterSalesRep] = useState('');

    const getData = async () => {
        const resp = await fetch('http://localhost:8090/api/records/');
        if (resp.ok) {
            const data = await resp.json();
            const salesRecords = data.sales_records.map((salesRecord) => {
                return {
                    id: salesRecord.id,
                    price: salesRecord.price,
                    vin: salesRecord.vin,
                    customer_name: salesRecord.customer_name,
                    sales_rep: `${salesRecord.sales_rep_name} ${salesRecord.sales_rep}`
                };
            });
            setSalesRecords(salesRecords);
            const reps = [];
            for (const salesRecord of salesRecords) {
                if (!reps.includes(salesRecord.sales_rep)) {
                    reps.push(salesRecord.sales_rep);
                }
            }
            setSalesReps(reps);
        }
    };

    useEffect(() => {
        getData();
    }, []);

    const handleSelectChange = (e) => {
        setFilterSalesRep(e.target.value);
    };

    return (
        <div className='container-responsive shadow p-4 mt-4'>
            <h2 className='text-center'>Sales Representative History</h2>
            <div className="mb-3">
                <select onChange={handleSelectChange} required name="salesRep" id="salesRep" className="form-select">
                    <option value="">Choose a sales representative</option>
                    {
                        salesReps.map(salesRep => {
                            const name = salesRep.replace(/[0-9]/g, '');
                            const id = salesRep.replace(/\D/g, '');
                            return (
                                <option key={id} value={id}>
                                    {name}
                                </option>
                            );
                        })
                    }
                </select>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Sales Representative</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {salesRecords
                        .filter( salesRecord => salesRecord.sales_rep.includes(filterSalesRep))
                        .map( salesRecord => {
                            const sales_rep_name = salesRecord.sales_rep.replace(/[0-9]/g, '');
                            return (
                                <tr key={salesRecord.id}>
                                    <td>{ sales_rep_name }</td>
                                    <td>{ salesRecord.customer_name}</td>
                                    <td>{ salesRecord.vin}</td>
                                    <td>{ salesRecord.price}</td>
                                </tr>
                            );
                        })
                    }
                </tbody>
            </table>
        </div>
    );
}

export default SalesRepHx;
