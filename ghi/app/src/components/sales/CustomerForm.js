import React, { useState } from 'react';

function CustomerForm() {
    const [formData, setFormData] = useState({
        name: '',
        address: '',
        city: '',
        state: '',
        zip_code: '',
        phone_number: ''
    });

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        const customersUrl = 'http://localhost:8090/api/customers/';

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            }
        };

        const response = await fetch(customersUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                name: '',
                address: '',
                city: '',
                state: '',
                zip_code: '',
                phone_number: ''
            });
        }
    };

    return (
        <div className="row">
            <div className="offset-3 col-6 mb-5 mt-5">
                <div className="shadow p-4 mt-3">
                    <h1 className='text-center'>Add Customer</h1>
                    <form onSubmit={handleSubmit} id="add-customer">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.name} placeholder='Name' required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.address} placeholder='Address' required type="text" name="address" id="address" className="form-control" />
                            <label htmlFor="address">Address</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.city} placeholder='City' required type="text" name="city" id="city" className="form-control" />
                            <label htmlFor="city">City</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.state} placeholder='State' required type="text" name="state" id="state" className="form-control" />
                            <label htmlFor="state">State</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.zip_code} placeholder='Zip Code' required type="text" name="zip_code" id="zip_code" className="form-control" />
                            <label htmlFor="zip_code">Zip Code</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.phone_number} placeholder='Phone number' required type="text" name="phone_number" id="phone_number" className="form-control" />
                            <label htmlFor="phone_number">Phone Number</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default CustomerForm;
