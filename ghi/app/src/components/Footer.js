import React from 'react';
import WomenOwned_logo_white from '../WomenOwned_Logo_white.png';
import gitlab from '../gitlab.png';
import './styles.css';

function Footer() {
    const padding = {
        paddingTop: '10px'
    }
    return (
        <>
            <div className='footer fixed-bottom mt-auto bg-dark text-white'>
                <div className='container'>
                    <div className='row'>
                    <div className='col d-inline bg-dark text-white'>
                            <img id="footer-logo" src={WomenOwned_logo_white} height='60px'></img>
                        </div>
                        <div className='col d-inline about-ae bg-dark text-white'>
                            <h5>Learn More</h5>
                            <p>Like our work? See our contact information and links to more of our work. </p>
                        </div>
                        <div className='col bios bg-dark'>
                            <h7 className='fw-bold'>Alix de Pannemaecker</h7>
                            <br />
                            <h7>adepannemaecker@gmail.com</h7>
                            <br />
                            <a href='https://www.linkedin.com/in/alix-de-pannemaecker-ph-d-511381147/' target="_blank" rel='noreferrer'><svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" className="bi bi-linkedin" viewBox="0 0 16 16">
  <path d="M0 1.146C0 .513.526 0 1.175 0h13.65C15.474 0 16 .513 16 1.146v13.708c0 .633-.526 1.146-1.175 1.146H1.175C.526 16 0 15.487 0 14.854V1.146zm4.943 12.248V6.169H2.542v7.225h2.401zm-1.2-8.212c.837 0 1.358-.554 1.358-1.248-.015-.709-.52-1.248-1.342-1.248-.822 0-1.359.54-1.359 1.248 0 .694.521 1.248 1.327 1.248h.016zm4.908 8.212V9.359c0-.216.016-.432.08-.586.173-.431.568-.878 1.232-.878.869 0 1.216.662 1.216 1.634v3.865h2.401V9.25c0-2.22-1.184-3.252-2.764-3.252-1.274 0-1.845.7-2.165 1.193v.025h-.016a5.54 5.54 0 0 1 .016-.025V6.169h-2.4c.03.678 0 7.225 0 7.225h2.4z"/>
</svg></a><a href='https://gitlab.com/Alix-de-Pannemaecker' target="_blank" rel='noreferrer'><img src={gitlab} width="38" height="38"/></a>
                        </div>
                        <div  className='col bios bg-dark'>
                            <h7 className='fw-bold'>Emily Arai</h7>
                            <br />
                            <h7 id="contact-info">emilyarai@gmail.com</h7>
                            <br />
                            <a href='https://www.linkedin.com/in/emily-arai-81751811/' target="_blank" rel='noreferrer'><svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" className="bi bi-linkedin" viewBox="0 0 16 16">
  <path d="M0 1.146C0 .513.526 0 1.175 0h13.65C15.474 0 16 .513 16 1.146v13.708c0 .633-.526 1.146-1.175 1.146H1.175C.526 16 0 15.487 0 14.854V1.146zm4.943 12.248V6.169H2.542v7.225h2.401zm-1.2-8.212c.837 0 1.358-.554 1.358-1.248-.015-.709-.52-1.248-1.342-1.248-.822 0-1.359.54-1.359 1.248 0 .694.521 1.248 1.327 1.248h.016zm4.908 8.212V9.359c0-.216.016-.432.08-.586.173-.431.568-.878 1.232-.878.869 0 1.216.662 1.216 1.634v3.865h2.401V9.25c0-2.22-1.184-3.252-2.764-3.252-1.274 0-1.845.7-2.165 1.193v.025h-.016a5.54 5.54 0 0 1 .016-.025V6.169h-2.4c.03.678 0 7.225 0 7.225h2.4z"/>
</svg></a><a href='https://gitlab.com/emi.rai' target="_blank" rel='noreferrer'><img src={gitlab} width="38" height="38"/></a>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Footer;
