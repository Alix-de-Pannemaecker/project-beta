import React, {useState, useEffect } from 'react';
import { Chart } from 'react-google-charts';

function VIPStats() {
    const [vipData, setVIPData] = useState([]);
    const [options, setOptions] = useState({
        title: "VIP clients using services",
    });

    const getData = async () => {
        const resp = await fetch('http://localhost:8080/api/technicians/3/appointments/');
        if (resp.ok) {
            const data = await resp.json();
            console.log(data.appointments);
            const vips = {
                "isVIP": 0,
                "notVIP": 0,
            }
            for (const appointment of data.appointments) {
                if (appointment.is_VIP === false) {
                    vips["notVIP"] ++;
                } else {
                    vips["isVIP"] ++;
                }
            }
            console.log(vips);
            const vipData = [
                ["VIP", "Not VIP"],
                ["VIP", vips["isVIP"]],
                ["Not VIP", vips["notVIP"]]
            ]
            setVIPData(vipData);
        }
    }
    useEffect(() => {
        getData();
    }, []);

    return (
        <>
            <div className='container shadow col mt-4'>
            <h1 className='text-center pt-3'>VIP Clients in Service</h1>
            <Chart
                chartType='PieChart'
                width='100%'
                height='400px'
                data={vipData}
                options={options}
            />
        </div>
        </>
    );
}

export default VIPStats;