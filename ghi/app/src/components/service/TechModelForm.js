import React, { useState } from 'react';

function TechModelForm() {

    const [name, setName] = useState('');
    const [employee_number, setEmployeeNumber] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        return setName(value);
    }
    const handlePictureChange = (event) => {
        const value = event.target.value;
        return setEmployeeNumber(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.employee_number = employee_number;

        const modelsUrl = `http://localhost:8080/api/technicians/`;

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(modelsUrl, fetchConfig);

        if (response.ok) {
            setName('');
            setEmployeeNumber('');
        }
    }

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6 mb-5 mt-5">
                    <div className="shadow p-4 mt-4">
                        <h1 className='text-center'>Add Technician</h1>
                        <form onSubmit={handleSubmit} id="create-auto-form">
                            <div className="form-floating mb-3">
                                <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="color">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={employee_number} onChange={handlePictureChange} placeholder="Employee number" required type="number" name="employee_number" id="employee_number" className="form-control" />
                                <label htmlFor="employee_number">Employee number</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );


}

export default TechModelForm;
