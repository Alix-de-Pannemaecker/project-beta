import React, { useState, useEffect } from 'react';

function AppointmentsModelList() {

    let [appointments, setAppointments] = useState([]);

    const fetchData = async () => {
        const resp = await fetch('http://localhost:8080/api/appointments/');
        if (resp.ok) {
            const data = await resp.json();
            setAppointments(data.appointments)
        }
        else {
            console.error("Bad request!")
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleCancel = async (appointment, e) => {
        const appointmentUrl = `http://localhost:8080/api/appointments/${appointment.id}/cancel/`
        const fetchConfig = {
            method: "put",
        }
        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            fetchData();
        }
    }

    const handleFinish = async (appointment, e) => {
        const appointmentUrl = `http://localhost:8080/api/appointments/${appointment.id}/finish/`
        const fetchConfig = {
            method: "put",
        }
        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            fetchData();
        }
    }

return (
    <div className="container-fluid shadow p-4">
        <h2 className="text-center p-2">Service Appointment Schedule</h2>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Customer</th>
                    <th scope="col">VIN</th>
                    <th scope="col">Date</th>
                    <th scope="col">Time</th>
                    <th scope="col">Assigned Technician</th>
                    <th scope="col">Reason</th>
                    <th scope="col">VIP</th>
                </tr>
            </thead>
            <tbody>
                {appointments.filter(e => !['CANCELED', 'FINISHED'].includes(e.status)).map(appointment => {
                    return (
                        <tr key={appointment.id}>
                            <td scope="row">{appointment.owner_name}</td>
                            <td scope="row">{appointment.vin}</td>
                            <td scope="row">{new Date(appointment.date_time).toLocaleDateString()}</td>
                            <td scope="row">{new Date(appointment.date_time).toLocaleTimeString([], { timeStyle: 'short' })}</td>
                            <td scope="row">{appointment.technician.name}</td>
                            <td scope="row">{appointment.reason}</td>
                            {(() => {
                                switch (appointment.is_VIP) {
                                    case true: return <td scope="row">&#10003;</td>;
                                    case false: return <td scope="row"></td>;
                                }
                            })()}
                            <td><button className="btn btn-secondary" onClick={e => handleCancel(appointment, e)}>Cancel</button></td>
                            <td><button className="btn btn-primary" onClick={e => handleFinish(appointment, e)}>Finish</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    </div>
    );
}

export default AppointmentsModelList;
