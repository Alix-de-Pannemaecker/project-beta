import React, { useState, useEffect } from 'react';

function AppointmentModelHistoryList() {

    let tmpValue = '';
    let [appointments, setAppointments] = useState([]);
    const [vin, setVin] = useState('');
    const [filterVin, setFilterVin] = useState("");

    const fetchData = async () => {
        const resp = await fetch('http://localhost:8080/api/appointments/');
        if (resp.ok) {
            const data = await resp.json();
            setAppointments(data.appointments);
            setVin(data.appointments.vin);
        }
        else {
            console.error("Bad request!")
        }

    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleVinChange = (event) => {
        tmpValue = event.target.value;
    }

    const handleSearch = (event) => {
        setFilterVin(tmpValue)
    }

    const filterFunction = (appointment) => {
        if (filterVin === '') {
            return true;
        } else if (filterVin === appointment.vin) {
            return true;
        }
    }

    return (
        <div className="shadow p-4 mt-4 container-fluid">
            <h2 className="text-center p-2">Service Appointment History</h2>
            <div className="input-group justify-content-center p-2">
                <div className="form-outline text-center">
                    <input placeholder="Enter vin" onChange={handleVinChange} type="search" id="vin" className="form-control" />
                </div>
                <button onClick={e => handleSearch(e)} type="button" className="btn btn-primary">Search</button>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Customer</th>
                        <th scope="col">VIN</th>
                        <th scope="col">Date</th>
                        <th scope="col">Time</th>
                        <th scope="col">Assigned Technician</th>
                        <th scope="col">Reason</th>
                        <th scope="col">VIP</th>
                        <th scope="col">Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.filter(filterFunction).map(appointment => {
                        let appointmentStatus = appointment.status.toLowerCase();
                        return (
                            <tr key={appointment.id}>
                                <td scope="row">{appointment.owner_name}</td>
                                <td scope="row">{appointment.vin}</td>
                                <td scope="row">{new Date(appointment.date_time).toLocaleDateString()}</td>
                                <td scope="row">{new Date(appointment.date_time).toLocaleTimeString([], { timeStyle: 'short' })}</td>
                                <td scope="row">{appointment.technician.name}</td>
                                <td scope="row">{appointment.reason}</td>
                                {(() => {
                                    switch (appointment.is_VIP) {
                                        case true: return <td scope="row">&#10003;</td>;
                                        case false: return <td scope="row"></td>;
                                    }
                                })()}
                                <td id='appointment-status' scope="row">{appointmentStatus}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default AppointmentModelHistoryList;
