import React, { useEffect, useState } from 'react';

function AppointmentModelForm() {

    const [name, setName] = useState('');
    const [vin, setVin] = useState('');
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');
    const [reason, setReason] = useState('');
    const [selectedTechnician, setSelectedTechnician] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        return setName(value);
    }
    const handleVinChange = (event) => {
        const value = event.target.value;
        return setVin(value);
    }
    const handleDateChange = (event) => {
        const value = event.target.value;
        return setDate(value);
    }
    const handleTimeChange = (event) => {
        const value = event.target.value;
        return setTime(value);
    }
    const handleReasonChange = (event) => {
        const value = event.target.value;
        return setReason(value);
    }
    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        return setSelectedTechnician(value);
    }

    const [technicians, setTechnicians] = useState([]);

    const fetchData = async () => {

        const url = 'http://localhost:8080/api/technicians/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.owner_name = name;
        data.vin = vin;
        data.date_time = new Date(date + 'T' + time).toISOString();
        data.reason = reason;
        data.technician = selectedTechnician;

        const appointmentUrl = `http://localhost:8080${selectedTechnician}appointments/`

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(appointmentUrl, fetchConfig);

        if (response.ok) {

            setName('');
            setVin('');
            setDate('');
            setTime('');
            setReason('');
            setSelectedTechnician('');
        }
    }

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6 mb-5 mt-5">
                    <div className="shadow p-4 mt-4">
                        <h1 className='text-center'>Create Appointment</h1>
                        <form onSubmit={handleSubmit} id="create-auto-form">
                            <div className="form-floating mb-3">
                                <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="color">Customer name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={vin} onChange={handleVinChange} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" />
                                <label htmlFor="year">Vin</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={date} onChange={handleDateChange} placeholder="Date" required type="date" name="date" id="date" className="form-control" />
                                <label htmlFor="year">Date</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={time} onChange={handleTimeChange} placeholder="Time" required type="time" name="time" id="time" className="form-control" />
                                <label htmlFor="year">Time</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={reason} onChange={handleReasonChange} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                                <label htmlFor="year">Reason</label>
                            </div>
                            <div className="mb-3">
                                <select value={selectedTechnician} onChange={handleTechnicianChange} placeholder="Choose a technician" required name="technician" id="technician" className="form-select">
                                    <option value="">Choose a technician</option>
                                    {technicians.map(technician => {
                                        return (
                                            <option key={technician.href} value={technician.href}>
                                                {technician.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default AppointmentModelForm;
