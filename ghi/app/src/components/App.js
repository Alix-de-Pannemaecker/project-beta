import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import NavBar from './NavBar';
import Footer from './Footer'
import AutomobilesList from './inventory/AutomobilesList';
import AutoForm from './inventory/AutoForm';
import VehicleModelsList from './inventory/VehicleModelsList';
import VehicleModelForm from './inventory/VehicleModelForm';
import ManufacturerList from './inventory/ManufacturerList';
import ManufacturerForm from './inventory/ManufactureForm';
import TechModelForm from './service/TechModelForm';
import AppointmentsModelList from './service/AppointmentsModelList';
import AppointmentModelHistoryList from './service/AppointmentModelHistoryList';
import AppointmentModelForm from './service/AppointmentModelForm';
import VIPStats from './service/VIPStats';
import SalesRepForm from './sales/SalesRepForm';
import CustomerForm from './sales/CustomerForm';
import SalesForm from './sales/SalesForm';
import SalesList from './sales/SalesList';
import SalesRepHx from './sales/SalesRepHx';
import CarsList from './rental/CarsList';
import CarForm from './rental/CarForm';
import BookingsList from './rental/BookingsList';
import BookingForm from './rental/BookingForm';
import SalesStats from './sales/SalesStats';

function App() {
  return (
    <>
    <BrowserRouter>
      <NavBar />
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="autos">
            <Route index element={<AutomobilesList />} />
            <Route path="new" element={<AutoForm />} />
          </Route>
          <Route path="models">
            <Route index element={<VehicleModelsList />} />
            <Route path="new" element={<VehicleModelForm />} />
          </Route>
          <Route path="manufacturers">
            <Route index element={<ManufacturerList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="technicians">
            <Route path="new" element={<TechModelForm />} />
          </Route>
          <Route path="appointments">
            <Route index element={<AppointmentsModelList />} />
            <Route path="history" element={<AppointmentModelHistoryList />} />
            <Route path="new" element={<AppointmentModelForm />} />
          </Route>
          <Route path="sales">
            <Route path="new" element={<SalesRepForm />} />
            <Route path="stats" element={<SalesStats />} />
          </Route>
          <Route path="customers">
            <Route path="new" element={<CustomerForm />} />
          </Route>
          <Route path="records">
            <Route index element={<SalesList />}/>
            <Route path="new" element={<SalesForm />}/>
            <Route path="history" element={<SalesRepHx />}/>
          </Route>
          <Route path="technicians">
            <Route path="new" element={<TechModelForm />} />
          </Route>
          <Route path="appointments">
            <Route index element={<AppointmentsModelList />} />
            <Route path="history" element={<AppointmentModelHistoryList />} />
            <Route path="new" element={<AppointmentModelForm />} />
            <Route path="stats" element={<VIPStats />} />
          </Route>
          <Route path="rentals">
            <Route path="cars" element={<CarsList />} />
            <Route path="cars/new" element={<CarForm />} />
            <Route path="bookings" element={<BookingsList />} />
            <Route path="bookings/new" element={<BookingForm />} />
          </Route>
        </Routes>
    </BrowserRouter>
    <Footer />
    </>
  );
}

export default App;
