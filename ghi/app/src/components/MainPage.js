import logo_ae_auto from '../logo_ae_auto.png'
import React from 'react';
import InventoryCarousel from './inventory/InventoryCarousel';
import ManufacturersColumn from './inventory/ManufacturersColumn';

function MainPage() {
  return (
    <>
    <div className="px-4 py-4 my-5 text-center">
      <h1 id="homepage-h1" className="fw-bold pb-2">A&E <br/>Autos</h1>
      <div className="col-lg-6 mx-auto">
        <h6 id="homepage-p" className="lead mb-5">
          The premiere solution for automobile dealership
          management!
        </h6>
        <br/>
        <InventoryCarousel />
        <ManufacturersColumn />
      </div>
    </div>
    </>
  );
}

export default MainPage;
