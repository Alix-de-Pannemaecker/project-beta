import React, { useState, useEffect } from 'react';
import Alert from 'react-bootstrap/Alert';


function CarForm() {

    const [price, setPrice] = useState('');
    const [selectedCar, setSelectedCar] = useState('');
    const [show, setShow] = useState(false);

    const handlePriceChange = (event) => {
        const value = event.target.value;
        return setPrice(value);
    }
    const handleCarChange = (event) => {
        const value = event.target.value;
        return setSelectedCar(value);
    }

    const [autos, setAutos] = useState([]);

    const fetchData = async () => {

        const url = 'http://localhost:8100/api/automobiles/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.price_per_day = price;
        data.auto = selectedCar;

        const carUrl = `http://localhost:8001/api/autos/`

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(carUrl, fetchConfig);

        if (response.ok) {

            setPrice('');
            setSelectedCar('');

        } else {

            setShow(true);

        }
    }

    return (
        <div className="container">
            <div className="row">
                {(() => {
                    switch (show) {
                        case true: return (
                            <Alert variant="danger" onClose={() => setShow(false)} dismissible>
                                <Alert.Heading>This car has already been sold!</Alert.Heading>
                                <p>
                                    Select another car.
                                </p>
                            </Alert>
                        );
                        case false: return (
                            <div className="offset-3 col-6 mb-5 mt-5">
                                <div className="shadow p-4 mt-4">
                                    <h1 className='text-center'>Create Car for Rent</h1>
                                    <form onSubmit={handleSubmit} id="create-auto-form">
                                        <div className="form-floating mb-3">
                                            <input value={price} onChange={handlePriceChange} placeholder="Price" required type="text" name="price" id="price" className="form-control" />
                                            <label htmlFor="color">Price per day</label>
                                        </div>
                                        <div className="mb-3">
                                            <select value={selectedCar} onChange={handleCarChange} placeholder="Choose a car" required name="car" id="car" className="form-select">
                                                <option value="">Choose a car</option>
                                                {autos.map(auto => {
                                                    return (
                                                        <option key={auto.vin} value={auto.vin}>
                                                            {auto.vin}
                                                        </option>
                                                    );
                                                })}
                                            </select>
                                        </div>
                                        <button className="btn btn-primary">Create</button>
                                    </form>
                                </div>
                            </div>
                        );
                    }
                })()}
            </div>
        </div>
    )

}

export default CarForm;
