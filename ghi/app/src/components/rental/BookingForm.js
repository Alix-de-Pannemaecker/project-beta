import React, { useState, useEffect } from 'react';
import Alert from 'react-bootstrap/Alert';

function BookingForm() {

    const [show, setShow] = useState(false);

    const [name, setName] = useState('');
    const [address, setAddress] = useState('');
    const [city, setCity] = useState('');
    const [code, setCode] = useState('');
    const [phone, setPhone] = useState('');
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');
    const [selectedCar, setSelectedCar] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        return setName(value);
    }
    const handleAddressChange = (event) => {
        const value = event.target.value;
        return setAddress(value);
    }
    const handleCityChange = (event) => {
        const value = event.target.value;
        return setCity(value);
    }
    const handleCodeChange = (event) => {
        const value = event.target.value;
        return setCode(value);
    }
    const handlePhoneChange = (event) => {
        const value = event.target.value;
        return setPhone(value);
    }
    const handleDateChange = (event) => {
        const value = event.target.value;
        return setDate(value);
    }
    const handleTimeChange = (event) => {
        const value = event.target.value;
        return setTime(value);
    }
    const handleCarChange = (event) => {
        const value = event.target.value;
        console.log(value)
        return setSelectedCar(value);
    }

    const [autos, setAutos] = useState([]);

    const fetchData = async () => {

        const url = 'http://localhost:8001/api/autos/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.address = address;
        data.city = city;
        data.zip_code = code;
        data.phone_number = phone;
        data.until = new Date(date + 'T' + time).toISOString();
        data.auto = selectedCar;

        const bookingUrl = `http://localhost:8001/api/autos/${selectedCar}/bookings/`

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(bookingUrl, fetchConfig);

        if (response.ok) {

            setName('');
            setAddress('');
            setCity('');
            setCode('');
            setPhone('');
            setDate('');
            setTime('');
            setSelectedCar('');

            handleRent(data.auto, event)

        } else {

            setShow(true);

        }

    }

    const handleRent = async (auto, e) => {
        const autoUrl = `http://localhost:8001/api/autos/${auto}/rented/`
        const fetchConfig = {
            method: "put",
        }
        const response = await fetch(autoUrl, fetchConfig);
        if (response.ok) {
            fetchData();
        }
    }

    return (
        <div className="container mb-5">
            <div className="row">
                {(() => {
                    switch (show) {
                        case true: return (
                            <Alert variant="danger" onClose={() => setShow(false)} dismissible>
                                <Alert.Heading>This car is already rented!</Alert.Heading>
                                <p>
                                    Select another car.
                                </p>
                            </Alert>
                        );
                        case false: return (
                            <div className="offset-3 col-6 mb-5 mt-5 pb-5">
                                <div className="shadow p-4 mt-4 pb-5">
                                    <h1 className='text-center'>Add Car Rental Booking</h1>
                                    <form onSubmit={handleSubmit} id="create-auto-form">
                                        <div className="form-floating mb-3">
                                            <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                            <label htmlFor="color">Customer name</label>
                                        </div>
                                        <div className="form-floating mb-3">
                                            <input value={address} onChange={handleAddressChange} placeholder="Address" required type="text" name="address" id="address" className="form-control" />
                                            <label htmlFor="color">Address</label>
                                        </div>
                                        <div className="form-floating mb-3">
                                            <input value={city} onChange={handleCityChange} placeholder="City" required type="text" name="city" id="city" className="form-control" />
                                            <label htmlFor="color">City</label>
                                        </div>
                                        <div className="form-floating mb-3">
                                            <input value={code} onChange={handleCodeChange} placeholder="Code" required type="text" name="code" id="code" className="form-control" />
                                            <label htmlFor="color">Zip code</label>
                                        </div>
                                        <div className="form-floating mb-3">
                                            <input value={phone} onChange={handlePhoneChange} placeholder="Phone" required type="text" name="phone" id="phone" className="form-control" />
                                            <label htmlFor="color">Phone number</label>
                                        </div>
                                        <div className="form-floating mb-3">
                                            <input value={date} onChange={handleDateChange} placeholder="Date" required type="date" name="date" id="date" className="form-control" />
                                            <label htmlFor="year">Date</label>
                                        </div>
                                        <div className="form-floating mb-3">
                                            <input value={time} onChange={handleTimeChange} placeholder="Time" required type="time" name="time" id="time" className="form-control" />
                                            <label htmlFor="year">Time</label>
                                        </div>
                                        <div className="mb-3">
                                            <select value={selectedCar} onChange={handleCarChange} placeholder="Choose a car" required name="car" id="car" className="form-select">
                                                <option value="">Choose a car</option>
                                                {autos.map(auto => {
                                                    return (
                                                        <option key={auto.id} value={auto.id}>
                                                            {auto.auto.vin_VO}
                                                        </option>
                                                    );
                                                })}
                                            </select>
                                        </div>
                                        <button className="btn btn-primary">Create</button>
                                    </form>
                                </div>
                            </div>
                        );
                    }
                })()}
            </div>
        </div>
    );

}

export default BookingForm;
