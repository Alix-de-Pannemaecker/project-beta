import React, { useState, useEffect } from 'react';

function CarsList() {

    let [autos, setAutos] = useState([]);

    const fetchData = async () => {
        const resp = await fetch('http://localhost:8001/api/autos/');
        if (resp.ok) {
            const data = await resp.json();
            setAutos(data.autos)
        }
        else {
            console.error("Bad request!")
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleShowRoom = async (auto, e) => {
        const autoUrl = `http://localhost:8001/api/autos/${auto.id}/showroom/`
        const fetchConfig = {
            method: "put",
        }
        const response = await fetch(autoUrl, fetchConfig);
        if (response.ok) {
            fetchData();
        }
    }

    const handleReturn = async (auto, e) => {
        const autoUrl = `http://localhost:8001/api/autos/${auto.id}/available/`
        const fetchConfig = {
            method: "put",
        }
        const response = await fetch(autoUrl, fetchConfig);
        if (response.ok) {
            fetchData();
        }
    }

    return (
        <div className="container-fluid shadow p-4">
            <h2 className="text-center p-2">Cars for Rent</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">VIN</th>
                        <th scope="col">$ per day</th>
                        <th scope="col">Status</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    {autos.map(auto => {
                        return (
                            <tr key={auto.id}>
                                <td scope="row">{auto.auto.vin_VO}</td>
                                <td scope="row">{auto.price_per_day}</td>
                                <td scope="row">{auto.status}</td>
                                <td><button className="btn btn-secondary" onClick={e => handleShowRoom(auto, e)}>Send to Show Room</button></td>
                                <td><button className="btn btn-primary" onClick={e => handleReturn(auto, e)}>Available</button></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
        );

}

export default CarsList;
