import React, { useState, useEffect } from 'react';

function BookingsList() {

    let [bookings, setBookings] = useState([]);

    const fetchData = async () => {
        const resp = await fetch('http://localhost:8001/api/bookings/');
        if (resp.ok) {
            const data = await resp.json();
            setBookings(data.bookings)
        }
        else {
            console.error("Bad request!")
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleReturn = async (auto, booking, e) => {
        const autoUrl = `http://localhost:8001/api/autos/${auto.id}/available/`
        const fetchConfig = {
            method: "put",
        }
        const response = await fetch(autoUrl, fetchConfig);
        if (response.ok) {
            fetchData();
        }

        handleDelete(booking, e)
    }

    const handleDelete = async (booking, e) => {
        const autoUrl = `http://localhost:8001/api/bookings/${booking}/`
        const fetchConfig = {
            method: "delete",
        }
        const response = await fetch(autoUrl, fetchConfig);
        if (response.ok) {
            fetchData();
        }
    }


    return (
        <div className="container-fluid shadow p-4">
            <h2 className="text-center p-2">Rentals booking list</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Customer</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Return Day</th>
                        <th scope="col">Return Time</th>
                        <th scope="col">VIN</th>
                        <th scope="col">Car Status</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    {bookings.filter(e => !['AVAILABLE', 'SHOWROOM'].includes(e.auto.status)).map(booking => {
                        let carStatus = booking.auto.status.toLowerCase();
                        return (
                            <tr key={booking.id}>
                                <td scope="row">{booking.name}</td>
                                <td scope="row">{booking.phone_number}</td>
                                <td scope="row">{new Date(booking.until).toLocaleDateString()}</td>
                                <td scope="row">{new Date(booking.until).toLocaleTimeString([], { timeStyle: 'short' })}</td>
                                <td scope="row">{booking.auto.auto.vin_VO}</td>
                                <td id='car-status' scope="row">{carStatus}</td>
                                <td><button className="btn btn-primary" onClick={e => handleReturn(booking.auto, booking.id, e)}>Return Car</button></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );

}

export default BookingsList;
