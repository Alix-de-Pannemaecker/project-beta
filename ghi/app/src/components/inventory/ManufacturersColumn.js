import React, { useState, useEffect } from 'react';

function ManufacturersColumn() {
    const [manufacturers, setManufacturers] = useState([]);

    const getManufacturerData = async() => {
        const resp = await fetch('http://localhost:8100/api/manufacturers/');
        if (resp.ok) {
            const data = await resp.json()
            console.log("DATA: ", data);
            console.log("MANUFACTURERS", data.manufacturers);
            setManufacturers(data.manufacturers);
            // need to map over manufactures to get names of manufacturers
            const names_manufacturer = manufacturers.map(manufacturer => manufacturer.name);
            console.log("MANUFACTURERS NAMES:", names_manufacturer)

        }
    }

    useEffect(() => {
        getManufacturerData()
    }, [])

    return (
            <>
                <h4 className='text-center pb-4 pt-8' id="our-brands">Our Brands</h4>
                <div className='container'>
                <div className='row row-cols-5' id="div-row-brands">
                        {
                            manufacturers.map(manufacturer => {

                                return (
                                    <div className='col bg-transparent mb-10'  key={manufacturer.id}>
                                        <div  className="card mb-3">
                                            <img src={manufacturer.logo_url} height='60vw' className="card-img-top" alt={manufacturer.name} />
                                        </div>
                                    </div>
                                );
                            })
                        }
                    </div>
            </div>
        </>
    )
}

export default ManufacturersColumn;