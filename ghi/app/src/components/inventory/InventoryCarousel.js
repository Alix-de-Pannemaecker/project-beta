import React, { useState, useEffect } from 'react';
import Carousel from 'react-bootstrap/Carousel';
import CarouselItem from 'react-bootstrap/esm/CarouselItem';

function InventoryCarousel() {
    const [autos, setAutos] = useState([]);
    const [models, setModels] = useState([]);


    const getAutoData = async () => {
        const resp = await fetch('http://localhost:8100/api/automobiles/');
        if (resp.ok) {
            const data = await resp.json();
            const autos = data.autos;
            setAutos(autos)
            const models = autos.map(auto => auto.model);
            setModels(models);
        }
        else {
            console.error("Bad request!")
        }
    }

    useEffect(() => {
        getAutoData();
    }, []);

    return (
        <>
            <h4 id='our-models'>Our Models</h4>
            <div className='container'>
                <Carousel>
                    {models.map(model => {
                        return (
                            <CarouselItem key={model.vin}>
                                <img
                                    className='d-block w-100 rounded rounded-lg'
                                    src={model.picture_url}
                                    width='50'
                                    key={model.vin}
                                />
                                {/* <Carousel.Caption>
                                </Carousel.Caption> */}
                            </CarouselItem>
                        );
                    })}
                </Carousel>
            </div>
        </>
    );
}

export default InventoryCarousel;