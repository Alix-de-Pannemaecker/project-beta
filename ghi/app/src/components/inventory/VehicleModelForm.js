import React, { useEffect, useState } from 'react';

function VehicleModelForm() {

    const [name, setName] = useState('');
    const [picture, setPicture] = useState('');
    const [selectedManufacturer, setSelectedManufacturer] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        return setName(value);
    }
    const handlePictureChange = (event) => {
        const value = event.target.value;
        return setPicture(value);
    }
    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        return setSelectedManufacturer(value);
    }

    const [manufacturers, setManufacturers] = useState([]);

    const fetchData = async () => {

        const url = 'http://localhost:8100/api/manufacturers/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.picture_url = picture;
        data.manufacturer_id = selectedManufacturer;

        //Sends data to the server:
        const modelsUrl = `http://localhost:8100/api/models/`;

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(modelsUrl, fetchConfig);

        if (response.ok) {

            setName('');
            setPicture('');
            setSelectedManufacturer('');
        }
    }

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6 mb-5 mt-5">
                    <div className="shadow p-4 mt-4">
                        <h1 className='text-center'>Add a Model</h1>
                        <form onSubmit={handleSubmit} id="create-auto-form">
                            <div className="form-floating mb-3">
                                <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="color">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={picture} onChange={handlePictureChange} placeholder="Picture" required type="url" name="picture" id="picture" className="form-control" />
                                <label htmlFor="year">Picture URL</label>
                            </div>
                            <div className="mb-3">
                                <select value={selectedManufacturer} onChange={handleManufacturerChange} placeholder="Choose a manufacturer" required name="manufacturer" id="manufacturer" className="form-select">
                                    <option value="">Choose a manufacturer</option>
                                    {manufacturers.map(manufacturer => {
                                        return (
                                            <option key={manufacturer.id} value={manufacturer.id}>
                                                {manufacturer.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default VehicleModelForm;
