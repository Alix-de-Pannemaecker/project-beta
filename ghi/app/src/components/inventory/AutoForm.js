import React, { useEffect, useState } from 'react';

function AutoForm() {

    const [color, setColor] = useState('');
    const [year, setYear] = useState('');
    const [vin, setVin] = useState('');
    const [selectedModel, setSelectedModel] = useState('');

    const handleColorChange = (event) => {
        const value = event.target.value;
        return setColor(value);
    }
    const handleYearChange = (event) => {
        const value = event.target.value;
        return setYear(value);
    }
    const handleVinChange = (event) => {
        const value = event.target.value;
        return setVin(value);
    }
    const handleModelChange = (event) => {
        const value = event.target.value;
        return setSelectedModel(value);
    }

    const [models, setModels] = useState([]);

    const fetchData = async () => {

        const url = 'http://localhost:8100/api/models/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setModels(data.models)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.color = color;
        data.year = year;
        data.vin = vin;
        data.model_id = selectedModel;

        const autoUrl = `http://localhost:8100/api/automobiles/`;

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(autoUrl, fetchConfig);

        if (response.ok) {

            setColor('');
            setYear('');
            setVin('');
            setSelectedModel('');
        }
    }

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6 mb-5 mt-5">
                    <div className="shadow p-4 mt-4">
                        <h1 className='text-center'>Add Vehicle to Inventory</h1>
                        <form onSubmit={handleSubmit} id="create-auto-form">
                            <div className="form-floating mb-3">
                                <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={year} onChange={handleYearChange} placeholder="Year" required type="number" name="year" id="year" className="form-control" />
                                <label htmlFor="year">Year</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={vin} onChange={handleVinChange} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" />
                                <label htmlFor="color">Vin</label>
                            </div>
                            <div className="mb-3">
                                <select value={selectedModel} onChange={handleModelChange} placeholder="Choose a model" required name="model" id="model" className="form-select">
                                    <option value="">Choose a model</option>
                                    {models.map(model => {
                                        return (
                                            <option key={model.id} value={model.id}>
                                                {model.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary mb-3">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default AutoForm;
