import React, { useState } from 'react';

function ManufacturerForm() {

    const [formData, setFormData] = useState({
        name: '',
    })

    const handleSubmit = async (e) => {
        e.preventDefault();

        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const resp = await fetch(manufacturerUrl, fetchConfig);

        if (resp.ok) {
            setFormData({
                name: '',
                fabric: '',
                style_name: '',
                color: '',
                location: '',
            });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    return (
        <>
            <div className='container'>
            <div className="row">
                <div className="offset-3 col-6 mb-5 mt-5">
                    <div className="shadow p-4 mt-4">
                        <h1 className='text-center pt-4 pb-4'>Add Manufacturer</h1>
                        <form onSubmit={handleSubmit} id="create-manufacturer">
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} value={formData.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="name">Manufacturer Name</label>
                            </div>
                            <button className="btn btn-primary mb-3">Create</button>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default ManufacturerForm;
