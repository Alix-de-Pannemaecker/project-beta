import React, { useState, useEffect } from 'react';

function AutomobilesList() {

    const [autos, setAutos] = useState([]);

    const fetchData = async () => {
        const resp = await fetch('http://localhost:8100/api/automobiles/');
        if (resp.ok) {
            const data = await resp.json();
            setAutos(data.autos)
        }
        else {
            console.error("Bad request!")
        }

    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="container-fluid ms-auto me-auto shadow row bg-light">
            <h1 className='text-center pt-5'>Vehicles</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">VIN</th>
                        <th scope="col">Color</th>
                        <th scope="col">Year</th>
                        <th scope="col">Model</th>
                        <th scope="col">Manufacturer</th>
                    </tr>
                </thead>
                <tbody>
                    {autos.map(auto => {
                        return (
                            <tr key={auto.id}>
                                <td scope="row">{auto.vin}</td>
                                <td scope="row">{auto.color}</td>
                                <td scope="row">{auto.year}</td>
                                <td scope="row">{auto.model.name}</td>
                                <td scope="row">{auto.model.manufacturer.name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default AutomobilesList;
