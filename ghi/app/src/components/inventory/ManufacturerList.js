import React, { useState, useEffect } from 'react';

function ManufacturerList() {
    const [manufacturers, setManufacturers] = useState([]);
    const getData = async () => {
        const resp = await fetch('http://localhost:8100/api/manufacturers/');
        if (resp.ok) {
            const data = await resp.json()
            setManufacturers(data.manufacturers);
        }
    }

    useEffect(() => {
        getData()
    }, [])

    return (
        <div className="row bg-secondary-subtle pb-5 mb-5">
            <div className="offset-3 col-6 mb-5">
                <div className="container-fluid shadow p-4 mt-4 pb-4">
                    <h1 className='text-center'>List Manufacturers</h1>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                            </tr>
                        </thead>

                            <tbody>
                                {
                                    manufacturers.map(manufacturer => {
                                        return (
                                            <tr key={manufacturer.id}>
                                                <td>{manufacturer.name}</td>
                                            </tr>
                                        )
                                    })
                                }

                            </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default ManufacturerList;
