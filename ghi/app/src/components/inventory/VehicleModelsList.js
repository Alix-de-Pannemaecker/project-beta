import React, { useState, useEffect } from 'react';

function VehicleModelsList() {

    let [models, setModels] = useState([]);

    const fetchData = async () => {
        const resp = await fetch('http://localhost:8100/api/models/');
        if (resp.ok) {
            const data = await resp.json();
            setModels(data.models)
        }
        else {
            console.error("Bad request!")
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="container-fluid ms-auto me-auto shadow row">
            <h1 className='text-center p-2'>Models</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Manufacturer</th>
                        <th scope="col">Picture</th>
                    </tr>
                </thead>
                <tbody>

                    {models.map(model => {
                        return (
                            <tr key={model.id}>
                                <td scope="row">{model.name}</td>
                                <td scope="row">{model.manufacturer.name}</td>
                                <td className="w-25">
                                    <img src={model.picture_url} className="img-fluid img-thumbnail" />
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default VehicleModelsList;
