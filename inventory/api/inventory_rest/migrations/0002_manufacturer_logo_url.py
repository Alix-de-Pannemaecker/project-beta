# Generated by Django 4.0.3 on 2023-03-13 19:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory_rest', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='manufacturer',
            name='logo_url',
            field=models.URLField(blank=True, null=True),
        ),
    ]
