# CarCar

Team:

* Alix de Pannemaecker - Automobile Service
* Emily Arai - Sales

## Design

3 microservices in this app:

1. Inventory:

    Back: already present, no modifications added.

    Front:

    - 3 lists:
        * AutomobilesList.js: lists available cars showing the vin, color, year, model, manufacturer.
        * VehicleModelList.js: lists available vehicles showing the name, manufacturer and model's picture.
        * ManufacturerList.js: lists available manufacturers showing the name

    - 3 forms:
        * AutoForm.js: creates a car with color, year, vin, select a specific model present in inventory.
        * VehicleModelForm.js: creates a model with name, picture url, select a specific manufacturer present in inventory.
        * ManufacturerForm.js: creates a manufacturer with name.

2. Service (see below)

3. Sales (see below)

## Service microservice

Back:

    - 1 poller: the automobiles from the inventory are polled and used as Value Objects displaying
    "import_href" that corresponds to a path and "vin_VO" that corresponds to a car's vin present in the
    inventory.

    - 3 models:
        * AutoVO: value object with import_href and vin_VO fields.
        * Status: name field that will need to be populated in the database later on with only 3 names:
        "BOOKED", "FINISHED", "CANCELED".
        * Appointment: owner_name, vin, date_time, reason, is_VIP (set as "false" by default) fields, a
        foreign key to the status and technician models. This model has a classmethod implemented allowing
        the status to be set as "BOOKED" when a new appointment created. Two other function are implemented:
        a "finish" function allows the status to be set at "FINISHED" in the database and a "cancel" function
        allows the status to be set at "CANCELED" in the database.

    - 7 functions in the views:
        * api_finish_appointment: sets appointment's status to "FINISHED" when called.
        * api_cancel_appointment: sets appointment's status to "CANCELED" when called.
        * get_auto_VO: gets all vin's of cars present in the inventory.
        * api_list_technicians: get and post methods on the list of technicians.
        * api_show_technician: delete, get and put methods on a technician object.
        * api_list_appointments: get and post methods on the list of appointments.
        * api_show_appointment: delete, get and put methods on an appointment object.
Front:

    - 2 forms:
        * TechModelForm.js: creates a new technician with name and unique employee number.
        * AppointmentModelForm.js: creates a new appointment customer's name, car's vin, date, time, reason,
        and the specific technician that will perform the service.

    - 2 lists:
        * AppointmentModelList.js: lists the appointments that are currently booked with customer's name,
        vin, date, time, reason, assigned technician. If the vin corresponds in a vin present in
        the inventory, a "VIP" statement is specificed with "YES" in green in the list, if not, it is set with
        "NO" in red in the list. A green "Finish" button will mark this appointment as "FINISHED" in the database,
        and a "Cancel" button will mark it as "CANCELED". Once a button clicked, the row desapears
        from the screen.
        * AppointmentModelHistory.js: lists the appointments history with customer's name, car's vin, date, time,
        assigned technician, reason, the VIP "YES" or "NO" status and the status "BOOKED", "CANCELED"
        or "FINISHED". A search button allows a filter on the vin number, displaying only the entries corresponding
        to this vin in the appointment's list.

## Sales microservice

The sales microservice has the following models: AutomobileVO, SalesRep, Customer, and SalesRecord.
AutomobileVO has a import_vin field that is synced via polling with the vin field on the Automobile in the inventory.

SalesRep has a name field, a unique employee_number and an href. It has a dunder string method that displays
the name field of the sales rep instance.

Customer has fields for name, address, city, state, zip_code, phone_regex, phone_number and an href. The phone_regex
field is used to validate the phone_number format. It has a dunder string field that displays the id customer instance.

SalesRecord has a price field, a one-to-one field that references the import_vin on the AutomobileVO model, a foreign
key that references the SalesRep, a foreign key referencing customer, and an href. It has a dunder string method that
displays the vin and the id of the sales record instance.
