from django.http import JsonResponse
from .models import AutoVO, Technician, Status, Appointment
from django.views.decorators.http import require_http_methods
import json
from .encoders import TechnicianListEncoder, TechnicianDetailEncoder, AutoVOListEncoder, AppointmentListEncoder, AppointmentDetailEncoder


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    """
    Lists the technicians names and the realted id parameter.

    Returns a dictionary with a single key "technicians" which
    is a list of links to the technicians, technicians names and ids.
    Each entry in the list is a dictionary that contains the name
    of the technician and the id for the technician's information.

    {
        "technicians": [
            {
                "href": URL to the technician,
                "name": technician's name,
                "id": technician's id,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianDetailEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the technician"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_technician(request, id):
    """
    Returns the details for the Technician model specified
    by the id parameter.

    This should return a dictionary with the link to the technician,
    the technician's name, employee number and the technician's id.

    {
        "href": URL to the technician,
        "name": the name of the technician,
        "employee_number": the employee number of the technician,
        "id": technician's id
    }
    """
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=id)
            return JsonResponse(
                technician,
                encoder=TechnicianDetailEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Thechnician does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(id=id).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
    else:
        try:
            content = json.loads(request.body)
            Technician.objects.filter(id=id).update(**content)
            technician = Technician.objects.get(id=id)
            return JsonResponse(
                technician,
                encoder=TechnicianDetailEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Technician does not exist"})
            response.status_code = 404
            return response


def get_auto_VO():
    autos_VO = AutoVO.objects.all()
    source = JsonResponse(
            {"autos_VO": autos_VO},
            encoder=AutoVOListEncoder,
        )
    return source.content


@require_http_methods(["GET", "POST"])
def api_list_appointments(request, id):
    """
    Lists the appointment vehicle owner names and the link to the
    appointment for the specified technician id.

    Returns a dictionary with a single key "appointments"
    which is a list of appointment vehicle owner names and URLS. Each
    entry in the list is a dictionary that contains the
    owner of the vehicle for that appointment, the car's vin number,
    the appointment's date, the assigned technician, the name of its status,
    the link to the appointment's information and the VIP status.

    {
        "appointments": [
            {
                "owner_name": appointment's title,
                "vin": vin's number,
                "date_time": date and time of the appointment,
                "technician": {
                    "href": URL to the technician,
                    "name": technician's name,
                    "id": technician's id
                },
                "is_VIP": returns true if vin exists in inventory, false otherwise,
                "id": appointment's id,
                "status": appointment's status name
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(id=id)
            content["technician"] = technician

        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician id"},
                status=400,
            )

        auto_VO = json.loads(get_auto_VO())
        for auto in auto_VO["autos_VO"]:
            if content["vin"] in auto["vin_VO"]:
                content["is_VIP"] = True

        appointment = Appointment.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_appointment(request, id=None):
    """
    Returns the details for the Appointment model specified
    by the id parameter.

    This should return a dictionary with the vehicle owner's name,
    the car's vin, the appointment's date, the appointment's time,
    the appointment's reason, the VIP status, its status name,
    and a dictionary that has the assigned technician's name and its URL.

    {
        "owner_name": appointment's title,
        "vin": vin's number,
        "date_time": date and time of the appointment,
        "reason": reason of the appointment,
        "technician": {
            "href": URL to the technician,
            "name": technician's name,
            "employee_number": employee number of the technician,
            "id": technician's id
        },
        "is_VIP": returns true if vin exists in inventory, false otherwise,
        "id": appointment's id,
        "status": appointment's status name
    },
    """
    if request.method == "GET":
        if id is not None:
            appointment = Appointment.objects.get(id=id)
            return JsonResponse(
                appointment,
                encoder=AppointmentDetailEncoder,
                safe=False,
            )
        else:
            appointment = Appointment.objects.all()
            return JsonResponse(
                {"appointments": appointment},
                encoder=AppointmentDetailEncoder,
                safe=False,
            )
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=id).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
    else:
        content = json.loads(request.body)
        try:
            if "technician" in content:
                technician = Technician.objects.get(name=content["technician"])
                content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician"},
                status=400,
            )

        try:
            if "status" in content:
                status = Status.objects.get(name=content["status"])
                content["status"] = status
        except Status.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid status"},
                status=400,
            )

        appointment = Appointment.objects.update(**content)

        appointment = Appointment.objects.get(id=id)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )


@require_http_methods(["PUT"])
def api_finish_appointment(request, id):
    appointment = Appointment.objects.get(id=id)
    appointment.finish()

    return JsonResponse(
        appointment,
        encoder=AppointmentDetailEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_cancel_appointment(request, id):
    appointment = Appointment.objects.get(id=id)
    appointment.cancel()

    return JsonResponse(
        appointment,
        encoder=AppointmentDetailEncoder,
        safe=False,
    )