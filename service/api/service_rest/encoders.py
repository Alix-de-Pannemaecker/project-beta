from common.json import ModelEncoder
from .models import AutoVO, Technician, Appointment


class AutoVOListEncoder(ModelEncoder):
    model = AutoVO
    properties = ["vin_VO", "import_href"]


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "id",
    ]


class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
        "id",
    ]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "owner_name",
        "vin",
        "date_time",
        "technician",
        "is_VIP",
        "id",
    ]
    encoders = {
        "technician": TechnicianListEncoder(),
    }

    def get_extra_data(self, o):
        return {"status": o.status.name}


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "owner_name",
        "vin",
        "date_time",
        "reason",
        "technician",
        "is_VIP",
        "id",
    ]
    encoders = {
        "technician": TechnicianDetailEncoder(),
    }

    def get_extra_data(self, o):
        return {"status": o.status.name}
