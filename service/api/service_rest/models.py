from django.db import models
from django.urls import reverse


class AutoVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin_VO = models.CharField(max_length=17, unique=True)


class Technician(models.Model):
    """
    The technician model represents an automotive technician
    """
    name = models.CharField(max_length=200, null=True, blank=True)
    employee_number = models.PositiveBigIntegerField(unique=True, null=True, blank=True)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_show_technician", kwargs={"id": self.id})


class Status(models.Model):
    """
    The Status model provides a status to an appointment, which
    can be BOOKED, FINISHED, or CANCELED.

    Status is a Value Object and, therefore, does not have a
    direct URL to view it.
    """
    name = models.CharField(max_length=10)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("id",)
        verbose_name_plural = "statuses"


class Appointment(models.Model):
    """
    The appointment model represents a car service appointment with an assigned technician
    """
    owner_name = models.CharField(max_length=200, null=True, blank=True)
    vin = models.CharField(max_length=17)
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200, null=True, blank=True)
    is_VIP = models.BooleanField(default=False)

    status = models.ForeignKey(
        Status,
        related_name="appointment",
        on_delete=models.PROTECT,
    )


    technician = models.ForeignKey(
        Technician,
        related_name="appointment",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_appointment", kwargs={"pk": self.id})

    def __str__(self):
        return self.owner_name

    def finish(self):
        status = Status.objects.get(name="FINISHED")
        self.status = status
        self.save()

    def cancel(self):
        status = Status.objects.get(name="CANCELED")
        self.status = status
        self.save()

    @classmethod
    def create(cls, **kwargs):
        kwargs["status"] = Status.objects.get(name="BOOKED")
        appointment = cls(**kwargs)
        appointment.save()
        return appointment

    class Meta:
        ordering = ("owner_name",)
