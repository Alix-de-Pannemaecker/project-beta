import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

# Import AutomobileVO from hats_rest
from sales_rest.models import AutomobileVO


def get_automobiles():
    try:
        response = requests.get("http://inventory-api:8000/api/automobiles/")
        content = json.loads(response.content)
        for automobile in content["autos"]:
            AutomobileVO.objects.update_or_create(
                import_vin=automobile["vin"]
            )
    except requests.ConnectionError as e:
        print(e, file=sys.stderr)


def poll():
    while True:
        print('Sales poller polling for data')
        try:
            get_automobiles()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
