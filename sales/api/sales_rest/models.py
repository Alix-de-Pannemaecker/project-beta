from django.db import models
from django.urls import reverse
from django.core.validators import RegexValidator


class AutomobileVO(models.Model):
    import_vin = models.CharField(max_length=17, unique=True)


class SalesRep(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveSmallIntegerField(unique=True)

    def get_api_url(self):
        return reverse("api_sales_rep", kwargs={"id": self.id})

    def __str__(self):
        return f"{self.name}"


class Customer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=150)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=150)
    zip_code = models.CharField(max_length=5)
    phone_regex = RegexValidator(
        regex=r'^\?1?\d{9,15}$',
        message="Phone number must be entered in the format: '999999999'.")
    phone_number = models.CharField(
        validators=[phone_regex],
        max_length=17, blank=True)

    def get_api_url(self):
        return reverse("api_customer", kwargs={"pk": self.id})

    def __str__(self):
        return f"{self.id}"


class SalesRecord(models.Model):
    price = models.PositiveIntegerField()
    vin = models.OneToOneField(
        AutomobileVO,
        related_name="sales_record_vin",
        on_delete=models.CASCADE,
    )
    sales_rep = models.ForeignKey(
        SalesRep,
        related_name="sales_record_rep",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="sales_record_customer",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_sales_record", kwargs={"id": self.id})

    def __str__(self):
        return f"{self.vin}, {self.id}"
