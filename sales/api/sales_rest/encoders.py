from .models import (
    AutomobileVO,
    SalesRep,
    SalesRecord,
    Customer
)
from common.json import ModelEncoder


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["import_vin"]


class SalesRepListEncoder(ModelEncoder):
    model = SalesRep
    properties = [
        "name",
        "employee_number",
    ]


class SalesRepDetailEncoder(ModelEncoder):
    model = SalesRep
    properties = [
        "name",
        "employee_number",
        "id"
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "address",
        "city",
        "state",
        "zip_code",
        "phone_number",
        "id",
    ]


class SalesRecordEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        "price",
        "vin",
        "sales_rep",
        "customer",
        "id",
    ]
    encoders = {
        "vin": AutomobileVOEncoder(),
        "sales_rep": SalesRepDetailEncoder(),
        "customer": CustomerEncoder(),
    }

    def get_extra_data(self, o):
        return {
            "vin": o.vin.import_vin,
            "sales_rep": o.sales_rep.employee_number,
            "sales_rep_name": o.sales_rep.name,
            "customer": o.customer.id,
            "customer_name": o.customer.name,
        }
