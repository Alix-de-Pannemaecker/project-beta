from django.urls import path

from .views import (
    api_list_sales_reps,
    api_sales_rep,
    api_customer,
    api_customer_list,
    api_sales_record,
    api_sales_record_list
    )


urlpatterns = [
    path("sales/", api_list_sales_reps, name="api_list_sales_reps"),
    path("sales/<int:id>/", api_sales_rep, name="api_sales_rep"),
    path("customers/", api_customer_list, name="api_customer_list"),
    path("customers/<int:id>/", api_customer, name="api_customer"),
    path("records/", api_sales_record_list, name="api_sales_record_list"),
    path("records/<int:id>/", api_sales_record, name="api_sales_record"),
]
