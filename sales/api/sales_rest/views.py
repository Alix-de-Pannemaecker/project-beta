from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import (
    AutomobileVO,
    SalesRep,
    Customer,
    SalesRecord
    )
from .encoders import (
    SalesRepDetailEncoder,
    SalesRepListEncoder,
    CustomerEncoder,
    SalesRecordEncoder
    )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_sales_rep(request, id):
    if request.method == "GET":
        try:
            sales_rep = SalesRep.objects.get(id=id)
            return JsonResponse(
                sales_rep,
                encoder=SalesRepDetailEncoder,
                safe=False
            )
        except SalesRep.DoesNotExist:
            response = JsonResponse(
                {"message": "No Sales Reps with that name"}
            )
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            sales_rep = SalesRep.objects.get(id=id)
            sales_rep.delete()
            return JsonResponse(
                sales_rep,
                encoder=SalesRepDetailEncoder,
                safe=False,
            )
        except SalesRep.DoesNotExist:
            response = JsonResponse(
                {"message": "No Sales Reps with that name"}
            )
            response.status_code = 404
            return response
    else:  # PUT
        try:
            content = json.loads(request.body)
            sales_rep = SalesRep.objects.get(id=id)

            props = ["name", "employee_number"]
            for prop in props:
                if prop in content:
                    setattr(sales_rep, prop, content[prop])
            sales_rep.save()
            return JsonResponse(
                sales_rep,
                encoder=SalesRepDetailEncoder,
                safe=False,
            )
        except SalesRep.DoesNotExist:
            response = JsonResponse(
                {"message": "Customer does not exist"}
            )
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_list_sales_reps(request):
    if request.method == "GET":
        sales_reps = SalesRep.objects.all()
        return JsonResponse(
            {"sales_reps": sales_reps},
            encoder=SalesRepListEncoder,
        )
    else:
        content = json.loads(request.body)
        sales_person = SalesRep.objects.create(**content)
        return JsonResponse(
            sales_person,
            encoder=SalesRepDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_customer(request, id):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=id)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "No customer with that name"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=id)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Customer does not exist"})
    else:  # PUT
        try:
            content = json.loads(request.body)
            customer = Customer.objects.get(id=id)

            props = ["name", "address", "city", "state", "zip_code"]
            for prop in props:
                if prop in content:
                    setattr(customer, prop, content[prop])
            customer.save()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Customer does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_customer_list(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_sales_record(request, id):
    if request.method == "GET":
        try:
            sales_record = SalesRecord.objects.get(id=id)
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except SalesRecord.DoesNotExist:
            return JsonResponse({"message": "Sales record does not exist"})
    if request.method == "DELETE":
        try:
            sales_record = SalesRecord.objects.get(id=id)
            sales_record.delete()
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except SalesRecord.DoesNotExist:
            return JsonResponse({"message": "Sales Record does not exist"})
    else:  # PUT
        try:
            content = json.loads(request.body)
            sales_record = SalesRecord.objects.get(id=id)

            props = ["price", "vin", "sales_rep", "customer"]
            for prop in props:
                if prop in content:
                    setattr(sales_record, prop, content[prop])
            sales_record.save()
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except SalesRecord.DoesNotExist:
            response = JsonResponse({"message": "Sales record does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_sales_record_list(request):
    if request.method == "GET":
        sales_records = SalesRecord.objects.all()
        return JsonResponse(
            {"sales_records": sales_records},
            encoder=SalesRecordEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            employee_number = content["sales_rep"]
            sales_rep = SalesRep.objects.get(employee_number=employee_number)
            content["sales_rep"] = sales_rep
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
            vin = content["vin"]
            automobile = AutomobileVO.objects.get(import_vin=vin)
            content["vin"] = automobile
            sales_record = SalesRecord.objects.create(**content)
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except SalesRep.DoesNotExist:
            response = JsonResponse(
                {"message": "Could not create the sales record"}
            )
            response.status_code = 400
            return response
