from django.apps import AppConfig


class RentalRestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'rental_rest'
