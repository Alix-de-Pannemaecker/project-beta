from django.db import models
from django.urls import reverse
from django.core.validators import RegexValidator


class AutoVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin_VO = models.CharField(max_length=17, unique=True)


class SoldAutoVO(models.Model):
    import_sold_href = models.CharField(max_length=200, unique=True)
    vin_sold_VO = models.CharField(max_length=17, unique=True)


class AvailableAutoVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin_VO = models.CharField(max_length=17, unique=True)


class Status(models.Model):
    """
    The Status model provides a status to a car booking, which
    can be "AVAILABLE", "SHOWROOM", "RENTED" or "SOLD".

    Status is a Value Object and, therefore, does not have a
    direct URL to view it.
    """
    name = models.CharField(max_length=10)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("id",)
        verbose_name_plural = "statuses"


class Car(models.Model):
    """
    The car model represents a car listed for rental
    """
    price_per_day = models.PositiveIntegerField(null=True, blank=True)

    auto = models.OneToOneField(
        AutoVO,
        related_name="car",
        on_delete=models.CASCADE,
    )

    status = models.ForeignKey(
        Status,
        related_name="car",
        on_delete=models.PROTECT,
    )

    def get_api_url(self):
        return reverse("api_car", kwargs={"pk": self.id})


    def rented(self):
        status = Status.objects.get(name="RENTED")
        self.status = status
        self.save()

    def available(self):
        status = Status.objects.get(name="AVAILABLE")
        self.status = status
        self.save()

    def show_room(self):
        status = Status.objects.get(name="SHOWROOM")
        self.status = status
        self.save()

    def sold(self):
        status = Status.objects.get(name="SOLD")
        self.status = status
        self.save()

    @classmethod
    def create(cls, **kwargs):
        kwargs["status"] = Status.objects.get(name="AVAILABLE")
        car = cls(**kwargs)
        car.save()
        return car

    class Meta:
        ordering = ("auto",)


class Booking(models.Model):
    """
    The booking model represents a car rental booking with a chosen available
    """
    name = models.CharField(max_length=200, null=True, blank=True)
    address = models.CharField(max_length=150)
    city = models.CharField(max_length=150)
    zip_code = models.CharField(max_length=5)

    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone_number = models.CharField(validators=[phone_regex], max_length=17, blank=True)

    until = models.DateTimeField()

    auto = models.OneToOneField(
        Car,
        related_name="booking",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_booking", kwargs={"pk": self.id})

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("name",)
