from django.urls import path
from .views import (
    api_list_autos,
    api_show_auto,
    api_rented_auto,
    api_showroom_auto,
    api_list_bookings,
    api_show_booking,
    api_available_auto,
)


urlpatterns = [
    path(
        "autos/",
        api_list_autos,
        name="api_list_autos",
    ),
    path(
        "autos/<int:id>/",
        api_show_auto,
        name="api_show_auto",
    ),
    path(
        "autos/<int:id>/bookings/",
        api_list_bookings,
        name="api_list_bookings",
    ),
    path(
        "bookings/<int:id>/",
        api_show_booking,
        name="api_show_booking",
    ),
    path(
        "bookings/",
        api_show_booking,
        name="api_show_booking",
    ),
    path(
        "autos/<int:id>/rented/",
        api_rented_auto,
        name="api_rented_auto",
    ),
    path(
        "autos/<int:id>/showroom/",
        api_showroom_auto,
        name="api_showroom_auto",
    ),
        path(
        "autos/<int:id>/available/",
        api_available_auto,
        name="api_available_auto",
    ),
]
