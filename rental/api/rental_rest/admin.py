from django.contrib import admin
from .models import Status, AvailableAutoVO, SoldAutoVO, Booking, Car, AutoVO


@admin.register(AvailableAutoVO)
class AvailableAutoVOAdmin(admin.ModelAdmin):
    pass


@admin.register(AutoVO)
class AutoVOAdmin(admin.ModelAdmin):
    pass


@admin.register(SoldAutoVO)
class SoldAutoVOAdmin(admin.ModelAdmin):
    pass


@admin.register(Status)
class StatusAdmin(admin.ModelAdmin):
    pass


@admin.register(Car)
class CarAdmin(admin.ModelAdmin):
    pass


@admin.register(Booking)
class BookingAdmin(admin.ModelAdmin):
    pass
