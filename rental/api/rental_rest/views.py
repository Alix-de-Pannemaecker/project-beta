from django.http import JsonResponse
from .models import Status, AutoVO, SoldAutoVO, AvailableAutoVO, Booking, Car
from django.views.decorators.http import require_http_methods
import json
from .encoders import SoldAutoVOListEncoder, BookingListEncoder, BookingDetailEncoder, CarListEncoder, AutoVOListEncoder
import traceback

def get_sold_auto_VO():
    sold_autos_VO = SoldAutoVO.objects.all()
    source = JsonResponse(
            {"sold_autos_VO": sold_autos_VO},
            encoder=SoldAutoVOListEncoder,
        )
    return source.content


def get_auto_VO():
    autos_VO = AutoVO.objects.all()
    source = JsonResponse(
            {"autos_VO": autos_VO},
            encoder=AutoVOListEncoder,
        )
    return source.content


@require_http_methods(["GET", "POST"])
def api_list_autos(request):
    if request.method == "GET":
        autos = Car.objects.all()
        return JsonResponse(
            {"autos": autos},
            encoder=CarListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            vin_content = content["auto"]
            auto_VO = AutoVO.objects.get(vin_VO=vin_content)
            content["auto"] = auto_VO

            try:
                status = Status(name="AVAILABLE")
                auto = Car.create(**content, status=status)
            except Exception as e:
                print("ERROR: ", e)
                print(traceback.format_exc())




            return JsonResponse(
                auto,
                encoder=CarListEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the car"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_auto(request, id):
    if request.method == "GET":
        try:
            auto = Car.objects.get(id=id)
            return JsonResponse(
                auto,
                encoder=CarListEncoder,
                safe=False
            )
        except Car.DoesNotExist:
            response = JsonResponse({"message": "Car does not exist"})
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        count, _ = Car.objects.filter(id=id).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
    else:
        content = json.loads(request.body)

        try:
            if "status" in content:
                status = Status.objects.get(name=content["status"])
                content["status"] = status
        except Status.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid status"},
                status=400,
            )

        auto = Car.objects.update(**content)

        auto = Car.objects.get(id=id)
        return JsonResponse(
            auto,
            encoder=CarListEncoder,
            safe=False,
        )

@require_http_methods(["PUT"])
def api_rented_auto(request, id):
    auto = Car.objects.get(id=id)
    auto.rented()

    return JsonResponse(
        auto,
        encoder=CarListEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_showroom_auto(request, id):
    auto = Car.objects.get(id=id)
    auto.show_room()

    return JsonResponse(
        auto,
        encoder=CarListEncoder,
        safe=False,
    )

@require_http_methods(["PUT"])
def api_available_auto(request, id):
    auto = Car.objects.get(id=id)
    auto.available()

    return JsonResponse(
        auto,
        encoder=CarListEncoder,
        safe=False,
    )


@require_http_methods(["GET", "POST"])
def api_list_bookings(request, id):
    if request.method == "GET":
        bookings = Booking.objects.all()
        return JsonResponse(
            {"booking": bookings},
            encoder=BookingListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            auto = Car.objects.get(id=id)
            content["auto"] = auto

        except Car.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid car id"},
                status=400,
            )

        booking = Booking.objects.create(**content)
        return JsonResponse(
            booking,
            encoder=BookingDetailEncoder,
            safe=False,
            )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_booking(request, id=None):
    if request.method == "GET":
        if id is not None:
            booking = Booking.objects.get(id=id)
            return JsonResponse(
                booking,
                encoder=BookingDetailEncoder,
                safe=False,
            )
        else:
            booking = Booking.objects.all()
            return JsonResponse(
                {"bookings": booking},
                encoder=BookingDetailEncoder,
                safe=False,
            )
    elif request.method == "DELETE":
        count, _ = Booking.objects.filter(id=id).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
    else:
        content = json.loads(request.body)
        try:
            if "auto" in content:
                auto = Car.objects.get(name=content["auto"])
                content["auto"] = auto
        except Car.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid car"},
                status=400,
            )

        try:
            if "status" in content:
                status = Status.objects.get(name=content["status"])
                content["status"] = status
        except Status.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid status"},
                status=400,
            )

        booking = Booking.objects.update(**content)

        booking = Booking.objects.get(id=id)
        return JsonResponse(
            booking,
            encoder=BookingDetailEncoder,
            safe=False,
        )