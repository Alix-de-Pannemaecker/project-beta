from common.json import ModelEncoder
from .models import AvailableAutoVO, AutoVO, SoldAutoVO, Booking, Car


class AutoVOListEncoder(ModelEncoder):
    model = AutoVO
    properties = ["vin_VO", "import_href"]


class SoldAutoVOListEncoder(ModelEncoder):
    model = SoldAutoVO
    properties = ["vin_sold_VO", "import_sold_href"]


class AvailableAutoVOListEncoder(ModelEncoder):
    model = AvailableAutoVO
    properties = ["vin_VO", "import_href"]


class CarListEncoder(ModelEncoder):
    model = Car
    properties = ["price_per_day", "auto", "id"]
    encoders = {
        "auto": AutoVOListEncoder(),
    }

    def get_extra_data(self, o):
        return {"status": o.status.name}


class BookingListEncoder(ModelEncoder):
    model = Booking
    properties = [
        "name",
        "until",
        "id",
        "auto",
    ]
    encoders = {
        "auto":CarListEncoder(),
    }


class BookingDetailEncoder(ModelEncoder):
    model = Booking
    properties = [
        "name",
        "address",
        "city",
        "zip_code",
        "phone_number",
        "until",
        "id",
        "auto",
    ]
    encoders = {
        "auto": CarListEncoder(),
    }