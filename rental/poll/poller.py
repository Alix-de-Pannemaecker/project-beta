import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "rental_project.settings")
django.setup()

from rental_rest.models import AutoVO, SoldAutoVO, AvailableAutoVO

def poll():
    while True:
        print('Rental poller polling for data from inventory')
        try:
            url = 'http://inventory-api:8000/api/automobiles/'

            response = requests.get(url)
            content = json.loads(response.content)

            for auto in content["autos"]:
                AutoVO.objects.update_or_create(
                    import_href=auto["href"],
                    defaults={"vin_VO": auto["vin"]},
                )

        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)

        print('Rental poller polling for data from sales')
        try:
            url = 'http://sales-api:8000/api/records/'

            response = requests.get(url)
            content = json.loads(response.content)

            for auto in content["sales_records"]:
                SoldAutoVO.objects.update_or_create(
                    import_sold_href=auto["href"],
                    defaults={"vin_sold_VO": auto["vin"]},
                )

        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)

        print('Rental poller polling for data from sales and inventory')
        try:
            url1 = 'http://inventory-api:8000/api/automobiles/'
            response1 = requests.get(url1)
            content1 = json.loads(response1.content)

            url2 = 'http://sales-api:8000/api/records/'
            response2 = requests.get(url2)
            content2 = json.loads(response2.content)

            for auto1 in content1["autos"]:
                for auto2 in content2["sales_records"]:
                    if auto1["vin"] not in auto2["vin"]:
                        AvailableAutoVO.objects.update_or_create(
                        import_href=auto1["href"],
                        defaults={"vin_VO": auto1["vin"]},
                        )

        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
